/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.interfaceanimal;

/**
 *
 * @author ASUS
 */
public class Dog extends LandAnimal{
    private String nickname;
    public Dog(String nickname) {
        super("Dog", 4);
        this.nickname = nickname;
    }

    @Override
    public void eat() {
        
    }

    @Override
    public void walk() {
        
    }

    @Override
    public void speak() {
        
    }

    @Override
    public void sleep() {
         
    }

    @Override
    public void run() {
        System.out.println("Dog: "+nickname+" fly");
    }
    
}
