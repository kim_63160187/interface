/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.interfaceanimal;

/**
 *
 * @author ASUS
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat bat = new Bat("Black");
        Plane plane = new Plane("Engine number 1");
        bat.fly(); // Animal, Poultry, Flyable
        plane.fly(); // Vehicle, Flyable
        Dog dog = new Dog("White");
        
        Flyable[] flyables = {bat, plane};
        for(Flyable f : flyables ){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        
        Runable[] runables ={dog, plane};
        for(Runable r : runables){
            r.run();
        }
    }
}
